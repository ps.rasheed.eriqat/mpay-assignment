# mPay-Assignment: (Technical)

### This assignment created to build mPay Technical knowledge, this assignment will include Real examples of isseus and requests from the clients handled by our engneers in the past.

# Goals and assignment desceiption:


### This assignment will be a scenario where the client needs to build a UAT environment for mPay system, apply recipes, and expand the infrastructure to include extra servers. In this assignment, you will work with a set of tools and scenarios to complete these tasks.

**Please Make sure to document all the steps for every part.**

# Phase 1: UAT Environment Details & Prerequisites

We Need To Configure the below :
- 1 VM with : <br>
  - **CentOS Linux release 7.8 (Core)** Operating System <br>

The below table provides the UAT VM Specifications :

| Servers | Specification | Configuration |
| ------ | ------ | ------ |
| mpay01.localdomain | 2cpu-2GB  memory |Redis, MINIO server, ActiveMQ   Tomcat and jdk  |

**you can use other Specification for your servers depnds on your HW** 

The Below table contains the apps versions to be downloaded:

| Tomcat9 | Redis | Minio server | ActiveMQ | JDK |
| ------ | ------ | ------ | ------ | ------ |
| Tomcat 9.0.45 | Redis v7.2 | Minio Server Latest version | ActiveMQ Classic 5.15.15 Release | OpenJDK 8 |


* *Use Vagrant to complete the setup, but without provision shells*
* *Assign static IP's in the Vagrant file from (vboxnet0) VirtualBox network interface*
---

# Prepare Database & Schema's:


## Download Oracle 19C Docker Image:

### hints: 

 You can use oracle docker image to practice this assignment : 

 * note that you may connect to progressoft docker hub  using below caommand 
 ```
   docker login -u psdeveloper -p 00fa32a8-ce58-4f8d-9f72-42956b3999d7

 ```

 * You can find last password from below link : 

 ```
   https://gitlab.com/progressoft/devops/genesis/k8s-configs
 ```

 ```


  docker run -d -e 'CHARACTER_SET=AR8MSWIN1256' -p 1521:1521 --restart=always --name Oracle-12c progressoft/oracle:db-19c-e-19.3.0

 ```

- Connect to the DB as sysdba

### Create Username and Password: (NCEP1_15_1)

- NCEP1_15_1:NCEP1_15_1
- Grant needed permissions

### Create Username and Password: (AciveMQ-classic)

- activemq:activemq
- Grant needed permissions

---

# Prepare the Schema:

### You have recived a DB dump from the DEV team to import it at the UAT client site, please make sure to import the dump into the new schema.

- Copy the dump into the oracle and import it.

Dump: https://progressoft-my.sharepoint.com/:u:/p/rasheed_eriqat/EVdVIXPJPulHtEr_QI2BFdkBvB-7dZQXxbRurK6mC4b54Q?e=9GsHR1

*You may face errors during importing the dump, please make sure to document all the errors and the resolution and the resolution for each*


---

# Prepare the Server:

> The client asks that we need to disable internet access to the servers.

- Disable internet access to the server and download/install the below pacakges offline:

  - **unzip,  zip, wget, tar, telnet, net-tools** and any other Package needed.

### Hint: disable internet access with iptables

* configure the ENV varibles in the bash profile and load it:

*use the below variables*

```
 User specific environment and startup programs
#-Dfile.encoding=UTF-8
PATH=$PATH:$HOME/.local/bin:$HOME/bin
export JAVA_OPTS="-server -d64 -Xms4096m -Xmx8192m  -Djava.security.egd=file:/dev/./urandom -XX:-UseGCOverheadLimit -Dfile.encoding=UTF8 -Djava.awt.headless=true -Duser.timezone=Asia/Ramallah"
export JAVA_HOME=path to jdk
export CATALINA_HOME= path to your tomcat
export db_check_query="select count(*) from user_tables"
export db_validation_query="SELECT 1 FROM DUAL"
export db_user="" # Your scheme username: mpay
export db_url="jdbc:oracle:thin:@jpay-mpaytestscan:1521/mpaytest"
export db_type="ORACLE10"
export db_schema=""
export db_password=""
export db_driver="oracle.jdbc.OracleDriver"
export db_dialect=org.hibernate.dialect.Oracle10gDialect
export REDIS_SERVICE=localhost # IP or hostname of Redis
export REDIS_PORT=  # Redis Port
export SPRING_URL=https://localhost:7070/psp/message  # Sping url
export env_broker_url="(tcp://IP:61616)
export MINIO_URL= # MINIOURL
export MINIO_ACCESS_KEY=minioadmin
export MINIO_SECRET_KEY=minioadmin
export CORE_URL=https://localhost:8443/mpay
export BRANCH_NAME='test'
export PATH=$JAVA_HOME/bin:$CATALINA_HOME/bin:$PATH


```
### Change the above variables based on below.

- Explain what is JAVA_OPTS and what can be configured in this variable.

1. Create user in the OS named: mpay

2. Change the hostname: mpay01.localdomain

3. Make sure that the HW Specification are met as the table above.

4. create dircotry in the root: u01

5. Unzip/uncommpress all the tools/apps in the above table in: u01

6. Change folder permissions and ownership to the *mpay* user.

7. Build the redis app(use make)

8. create jks contains self-signd certificate with alias: Tomcat-ssl-2024

9. Configure Tomcat to wotk with ssl, over port 8443

10. run Redis server over port: 6380

11. Run Minio Server over port: 9090

    - Config the MINIO Store path in u01

* Make smok tests to all the apps:

    - Enter the WebUI for tomcat and minio

12. Unzip ActiveMQ classic:*
 
 * Configure ActiveMQ to use Oracle to store all the data (**Use ActiveMQ Schema**).


---

# After the successful installation and configurations:

1. Create a dir in the: *u01* named: **mPay-controller**.

2. Download the WAR & Jar files from here: https://progressoft-my.sharepoint.com/:f:/p/rasheed_eriqat/ErQfLD3hwgpCmG65Ez_nJhIBq80o7N4k7mcO3iDzD8RQtA?e=a6K47R

3. Deploy the war file in tomcat with name: mpay

4. Deploy the Jar file in **mPay-controller**, and change the application properties file in the jar.

---


# Phase 2: PROD Environment Details & Prerequisites

### After Smok test and other tests conducted by the client, the client accepts to go-live with mpay, so in order to to that the client infom us that we need to expand the current UAT site to go live with mpay system, new server will be added(Replication of the server1)

### New server called: loadbalancer.localdomain to act as loadbalancer.

We Need To Configure the below :
- 2 VM with : <br>
  - **CentOS Linux release 7.8(Core)** Operating System <br>

The below table provides the PROD VM Specifications :

| Servers | Specification | Configuration |
| ------ | ------ | ------ |
| mpay01.localdomain (Ready)| 2cpu-2GB  memory |Redis, MINIO server, ActiveMQ   Tomcat and jdk  |
| mpay02.localdomain (New)| 2cpu-2GB  memory |Redis, MINIO server, ActiveMQ   Tomcat and jdk  |
| loadbalancer.localdomain (New) | 2cpu-2GB  memory |HAproxy or Nginx  |

**You can customize Specification for your servers depnds on your HW** 


The Below table contains the tools\apps versions to be downloaded:

| Tomcat9 | Redis | Minio server | ActiveMQ | JDK |
| ------ | ------ | ------ | ------ | ------ |
| Tomcat 9.0.45 | Redis v7.2 | Minio Server Latest version | ActiveMQ Classic 5.15.15 Release | OpenJDK 8 |


> The client asks that we need to disable internet access to the servers excepts the **LoadBalancer Server**

- Disable internet access to the server and download/install the below pacakges offline:

  - **unzip,  zip, wget, tar, telnet, net-tools** and any other Package needed.


**Draw the new infrastructre(with all details)**



---

# Next Step:

### Now after installing and configuring the above new servers we need configure the new app server: mpay02.localdomain exactly as the server 1 configured, Also deploy mPay system and make sure that no errors.


## Configure LoadBalancer Server:

1. Configure a web server such as Nginx or HAproxy.
2. Publish the web server over HTTPS port.
3. Route all traffic to the apps behind the load balancer to HTTPS port (the load balancer will forward requests to the HTTPS ports on the Tomcat servers).
4. Test the load balancer by accessing the IP over HTTPS port.
5. Use the Vagrant SSH command to forward ports from the box to the host.
6. Configure the web server to work with sticky sessions.
7. Consider other configurations that might be necessary for the load balancer.

---

### ActiveMQ Classic: Acrive-Passive setup

- Enable ActiveMQ Classic to operate as active-passive setup.


# Security Enhancmnts:

### In the proccess of going live, the client needs to ensure security measurs are taken, in order to do that the client asks for the below:


- Make sure to configure Firewall and iptabels to block all the un-needed traffic and allow the traffic only over apps ports.

- Publish ActiveMQ over HTTPS port.

- Publish MINIO over HTTPS, and use the new server to access it from the LB.

 * Try to configure the Webserver(Loadbalancer) to access ActiveMQ UI and Minio using the new HTTPS ports.

- Enable access logging in MINIO

- Change the default username and password for the below:
1. ActiveMQ
2. MINIO

- Upgrade the Tomcat version to version that mitigate the below VAPT (vualnrabilty) reported by client security team:

-> **Important: Denial of Service CVE-2021-30639**



---


# Final Step:

### Make sure that the site is up and running and no issues in the connectivity.

### Perform Site certification, use excel sheet attached.

- Explain what is site certification.


# Now the system is live, and below we have several cases reported by the client to enhance the site:


### Case 1: 

> The Client Reported that Tomcat log file is geeting bigger in size and they cant read it properly, client needs to resolve this issue, please suggest a solution to resolve this issue.

- Consider Archiving (create script to do that)

### Case 2:

> The client needs to prevent the single point of failur in the infrasturcure.

- Please suggest what can be enhanced to prevent the single point of failur, and what is the confguration needed.

> Also the client request to add a NFS server to mount the minio Path on it, please suggest a solution to do that

- Create new server as below:

| Servers | Specification | Configuration |
| ------ | ------ | ------ |
|nfs01.localdomain | 2cpu-2GB  memory | NFS |

- Mount the Minio Path on the 2 mPay servers to the new NFS server

- Document all the steps.

- Draw new site.

### Case 3:

> The Client needs to upgrade ActiveMQ to ActiveMQ Artemis latest version, and enabling HA setup using the Oracle DB 


- Since Progressoft unifies the deployment of such components by providing a pre-made recipe, please apply the following recipe to the two application servers.

https://gitlab.com/progressoft/recipes/baremetal/artemis

**Please do not use NGINX in the setup**

**Make sure to create new schema for the Artemis**

### Case 4:

> The client needs to control the time in all servers, since they have summer and wnter time changes, and they want to create NTP server.

- Describe what is NTP and how we can configure NTP.

- Create another server as below:

| Servers | Specification | Configuration |
| ------ | ------ | ------ |
|ntp-server01.localdomain | 2cpu-2GB  memory | NTP |

- This server will control the time in all servers, chnage the time from the NTP server and check if the time changed on all servers.

- Document all the steps.

- Draw new site.


> Now the client needs to change the time on all servers to be +1 hour, but they need to keep the mPay application and all other components running in -1 hour time

#### -For example: the client needs to switch from winter time to summer time.

- Lets assume Time on server is:  **Mon Apr  1 07:19:40 AM +03 2024**
 
- We need to change the time to be **Mon Apr  1 08:19:40 AM +03 2024** which is +1 hour, use Timezone or any other way to change the time

* now all the servers must be synced with the new time, but mPay system must run and show the old time before switch to summer time.

- How can we apply that? 

- Apply the suggested solution if you can.



### Case 5:

> The client is facing issues in Tomcat connectivity (When Tomcat is down the client face downtime and this is very critical) we need a solution to prevent such issue, and to monitor the tomcat service connectivity and report issues at the time they occured.

- Please apply the below Health Check script for the tomcat servers.

https://gitlab.com/progressoft/recipes/baremetal/bash


> Also the client needs the Tomcat servers to start at the startup of the server.

- Please suggest a solution for the above request, and implement it.


### Request 1:

> create a read-only user in Oracle


# Phase 3: Build UAT site with PostgresQL 14:

### The client decides that they need to build new UAT site with 1 app server and 1 DB server with Postgres 14 insted of oracle, so we need to do the following:

We Need To Configure the below :
- 2 VM with : <br>
  - **CentOS Linux release 7.8(Core)** Operating System <br>

### The below table provides the UAT VM Specifications :

| Servers | Specification | Configuration |
| ------ | ------ | ------ |
| mpay01-test.localdomain (New)| 2cpu-2GB  memory |Redis, MINIO server, ActiveMQ   Tomcat and jdk  |
| postgres-test.localdomain (New)| 2cpu-2GB  memory | Postgres 14 |

- Disable internet access to the server and download/install the below pacakges offline:

  - **unzip,  zip, wget, tar, telnet, net-tools** and any other Package needed.

### Hint: Use Iptables

### Install and Configure Postgres DB:

1. Install Postgres 14 on the standalone DB server (BareMetal Installation)
2. Configure Postgres to ensure proper database setup and security measures.

    - Configure the Postgres server to only accept connections from the app server.


3. Create a new database for the UAT environment.

    - Create New Database,schema and user.

4. Replicate App Server for UAT.

Replicate one of the existing application servers to be used for UAT.
Ensure that the replicated server is configured identically to the production environment, except for the database connection details.

### Deploy mPay on UAT:

- Please follow the below steps:

1. Create a dir in the: *u01* named: **mPay-controller**.

2. Download the WAR & Jar files from here: https://progressoft-my.sharepoint.com/:f:/p/rasheed_eriqat/ErQfLD3hwgpCmG65Ez_nJhIBq80o7N4k7mcO3iDzD8RQtA?e=a6K47R

3. Deploy the war file in tomcat with name: mpay

4. Deploy the Jar file in **mPay-controller**, and change the application properties file in the jar.

5. use the below enviroment variables and modify them as needed:

```
export db_type="POSTGRESSQL"
#export db_dialect="com.progressoft.backup.dialect.PostgreSQLBackupDialect"
export db_dialect="org.hibernate.dialect.PostgreSQLDialect"
export db_driver="org.postgresql.Driver"
export db_user="mpay"
export db_url="jdbc:postgresql://localhost:5432/ncep"
export db_check_query="select 1"
export db_schema="NCEP_1"
export db_validation_query="select 1"
export db_password="mpay"
export CATALINA_HOME="/home/rasheed/projects/ECHQ-Setup/apache-tomcat-9.0.85"
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
export JAVA_OPTS="-server -Xms1024m -Xmx4096m -XX:PermSize=128m -XX:MaxPermSize=2048m"


```


### To build the database with the initlizer, we need to to the following:

1. ensure that database, schema and user are created in postgres.
2. source the varibales above after modifications.
3. Navigate to the below dir:
> WEB-INF/lib

4. Execute the blow command to run the initlizer:

```
java -jar initializer-*.jar *entities*.jar
```

5. Open the **startup.properties** file in **WEB-INF/properties** and change the below value to true:

> setup.enabled=false

### The above steps will create new database with tables and objects, and step 5 will create basic objects like sys user.


# Now the UAT site is up, and below we have several request by the client to enhance the site:


### Request 1:

> create a read-only user in PostgreSQL (on mPay schema)

### Request 2:

> Build DR DB for UAT (Postgres)

### Request 3:

> Write bash script to export dump from givin schema name and import it in a given schema name (or just import dump and save the dump file based on user request) 

### Request 4:

> Write procedure to upgrade the postgres from 14 to latest version (for example: 16) and try to implemt it and do smok test on application.


# This is the end of this assignment, good luck!
